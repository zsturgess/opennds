## 1. The openNDS project

openNDS (open Network Demarcation Service) is a high performance, small footprint, Captive Portal.

It provides a border control gateway between a public local area network and the Internet.

It supports all ranges between small stand alone venues through to large mesh networks with multiple portal entry points.

Both the client driven Captive Portal Detection (CPD) method and gateway driven Captive Portal Identification method (CPI - RFC 8910 and RFC 8908) are supported.

In its default configuration, openNDS offers a dynamically generated and adaptive splash page sequence. Internet access is granted by a click to continue button, accepting Terms of Service. A simple option enables input forms for user login.

The package incorporates the FAS API allowing many flexible customisation options.

The creation of sophisticated third party authentication applications is fully supported.

Internet hosted **https portals** can be implemented with no security errors, to inspire maximum user confidence.

## 2. This fork

This project is forked from the [upstream openNDS project](https://github.com/openNDS/openNDS).

The goal of this fork is to support eaiser deployment on Ubuntu LTS 22.04 by:
 - Stripping files related to deployment on openWRT
 - Backporting fixes from modern versions of openNDS to v9.9.1, the last version that reliably supports iptables.
 - Provide a .deb for easy installation

